﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CurrencyConverterSed
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new MainPage();
		}

		protected override void OnStart()
		{
			var BC = MainPage.BindingContext as MainViewModel;
			BC.ConvertCommandExe();

		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
		}
	}
}

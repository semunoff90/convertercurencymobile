﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows.Input;
using System.Text.Json;
using Xamarin.Forms;

namespace CurrencyConverterSed
{
	[DesignTimeVisible(true)]
	class MainViewModel : INotifyPropertyChanged
	{

		public MainViewModel()
		{
			_text = "";
			_activInd = false;
			_currencysF = new ObservableCollection<CurrencyInfo>();
			_selectedCurrencyF = null;
			_selectedCurrencyS = null;
			_nominalF = 1;
			_nominalS = 1;
			_selectedDate = DateTime.Now;
			_idSelectedF = -1;
			_idSelectedS = -1;
		}

		private string _text;
		private bool _activInd;
		private ObservableCollection<CurrencyInfo> _currencysF;
		private CurrencyInfo _selectedCurrencyF;
		private CurrencyInfo _selectedCurrencyS;
		private double _nominalF;
		private double _nominalS;
		private DateTime _selectedDate;
		private int _idSelectedF;
		private int _idSelectedS;

		//-----------------------------------------------------------------------------------------------------------------------
		public int IdSelectedF
		{
			get => _idSelectedF;
			set
			{
				_idSelectedF = value;
				OnPropertyChanged(nameof(IdSelectedF));
			}
		}

		public int IdSelectedS
		{
			get => _idSelectedS;
			set
			{
				_idSelectedS = value;
				OnPropertyChanged(nameof(IdSelectedS));
			}
		}

		public DateTime SelectedDate
		{
			get => _selectedDate;
			set
			{
				_selectedDate = value;
				OnPropertyChanged(nameof(SelectedDate));
			}
		}

		public double NominalF
		{
			get => _nominalF;
			set
			{
				if (_nominalF == value) return;
				if (SelectedCurrencyS == SelectedCurrencyF)
				{
					value = 1;
				}
				_nominalF = value;
				OnPropertyChanged(nameof(NominalF));
				//if (SelectedCurrencyS != null && SelectedCurrencyF != null) SetNominalFExe();
			}
		}

		public double NominalS
		{
			get => _nominalS;
			set
			{
				if (_nominalS == value) return;
				if (SelectedCurrencyS == SelectedCurrencyF)
				{
					value = 1;
				}
				_nominalS = value;
				OnPropertyChanged(nameof(NominalS));
				//if (SelectedCurrencyS != null && SelectedCurrencyF != null) SetNominalSExe();
			}
		}

		public string Text
		{
			get => _text;
			set
			{
				_text = value;
				OnPropertyChanged(nameof(Text));
			}
		}

		public bool ActivInd
		{
			get => _activInd;
			set
			{
				_activInd = value;
				OnPropertyChanged(nameof(ActivInd));
			}
		}

		public ObservableCollection<CurrencyInfo> CurrencysF
		{
			get => _currencysF;
			set
			{
				_currencysF = value;
				OnPropertyChanged(nameof(CurrencysF));
			}
		}

		public CurrencyInfo SelectedCurrencyF
		{
			get => _selectedCurrencyF;
			set
			{
				_selectedCurrencyF = value;
				OnPropertyChanged(nameof(SelectedCurrencyF));
				if(SelectedCurrencyS != null && _selectedCurrencyF != null) SetNominalFExe();
			}
		}

		public CurrencyInfo SelectedCurrencyS
		{
			get => _selectedCurrencyS;
			set
			{
				_selectedCurrencyS = value;
				OnPropertyChanged(nameof(SelectedCurrencyS));
				if (SelectedCurrencyF != null && _selectedCurrencyF != null) SetNominalFExe();
			}
		}


		//================================================================================================================================

		private ICommand _convertCommand;
		private ICommand _setNominalF;
		private ICommand _setNominalS;

		//-----------------------------------------------------------------------------------------------------------------------

		public ICommand ConvertCommand => _convertCommand ?? (_convertCommand = new RelayCommand(ConvertCommandExe));
		public ICommand SetNominalF => _setNominalF ?? (_setNominalF = new RelayCommand(SetNominalFExe));
		public ICommand SetNominalS => _setNominalS ?? (_setNominalS = new RelayCommand(SetNominalSExe));


		private void SetNominalFExe()
		{
			NominalS = Math.Round(NominalF * (SelectedCurrencyF.Value / SelectedCurrencyF.Nominal) / SelectedCurrencyS.Value, 2);
		}

		private void SetNominalSExe()
		{
			NominalF = Math.Round(NominalS * (SelectedCurrencyS.Value / SelectedCurrencyS.Nominal) / SelectedCurrencyF.Value, 2);
		}

		public async void ConvertCommandExe()
		{
			Text = "Hi";
			ActivInd = true;
			var httpClient = new HttpClient();
			HttpResponseMessage response = null;
			if (SelectedDate == DateTime.Now)
			{
				response = await httpClient.GetAsync("https://www.cbr-xml-daily.ru/daily_json.js");
			}
			else
			{
				response = await httpClient.GetAsync($"https://www.cbr-xml-daily.ru/archive/{SelectedDate.Year}/{SelectedDate.Month}/{SelectedDate.Day}/daily_json.js");
			}

			int i = 0;
			while (!response.IsSuccessStatusCode)
			{
				SelectedDate = SelectedDate.AddDays(-1);
				i++;
				if (i == 62) SelectedDate = DateTime.Now;
				response = await httpClient.GetAsync($"https://www.cbr-xml-daily.ru/archive/{SelectedDate.Year}/{SelectedDate.Month}/{SelectedDate.Day}/daily_json.js");
			}

			var content = await response.Content.ReadAsStreamAsync();
			var result = await JsonSerializer.DeserializeAsync<DailyRate>(content);
			var tmpf = IdSelectedF;
			var tmps = IdSelectedS;
			SelectedCurrencyF = null;
			SelectedCurrencyS = null;
			CurrencysF.Clear();
			var RUB = new CurrencyInfo();
			RUB.Value = 1;
			RUB.CharCode = "RUB";
			RUB.Nominal = 1;
			CurrencysF.Add(RUB);
			CurrencysF.Add(result.Valute.AUD);
			CurrencysF.Add(result.Valute.AZN);
			CurrencysF.Add(result.Valute.GBP);
			CurrencysF.Add(result.Valute.USD);
			CurrencysF.Add(result.Valute.EUR);
			if(tmpf != -1) SelectedCurrencyF = CurrencysF[tmpf];
			if (tmps != -1) SelectedCurrencyS = CurrencysF[tmps];
			try
			{
				SetNominalFExe();
			}
			catch (Exception e)
			{
				
			}
			ActivInd = false;
		}

		//================================================================================================================================
		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	class CurrencyInfo
	{
		public string CharCode { get; set; }
		public int Nominal { get; set; }
		public double Value { get; set; }
	}

	class Currency
	{
		public Currency()
		{

		}

		private CurrencyInfo _aut;
		private CurrencyInfo _azn;
		private CurrencyInfo _gbp;
		private CurrencyInfo _usd;
		private CurrencyInfo _eur;

		public CurrencyInfo USD
		{
			get => _usd;
			set
			{
				_usd = value;
				OnPropertyChanged(nameof(USD));
			}
		}
		public CurrencyInfo EUR
		{
			get => _eur;
			set
			{
				_eur = value;
				OnPropertyChanged(nameof(EUR));
			}
		}
		public CurrencyInfo AUD
		{
			get => _aut;
			set
			{
				_aut = value;
				OnPropertyChanged(nameof(AUD));
			}
		}
		public CurrencyInfo AZN
		{
			get => _azn;
			set
			{
				_azn = value;
				OnPropertyChanged(nameof(AZN));
			}
		}
		public CurrencyInfo GBP
		{
			get => _gbp;
			set
			{
				_gbp = value;
				OnPropertyChanged(nameof(GBP));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	class DailyRate
	{
		public DateTime Date { get; set; }
		public DateTime PreviousDate { get; set; }
		public string PreviousURL { get; set; }
		//public Currency Valute { get; set; }
		public Currency Valute { get; set; }

	}
}
